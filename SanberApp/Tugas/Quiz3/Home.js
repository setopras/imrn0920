import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const HomeScreen = () => {
  return (
    <ScrollView style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <TextInput
          style={{
            borderRadius: 11,
            borderWidth: 1,
            borderColor: '#727C8E',
            width: '90%',
          }}
        />
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 5,
          }}>
          <Icon name="bell" size={25} />
        </View>
      </View>
      <View style={{justifyContent: 'center', marginVertical: 16}}>
        <Image
          source={require('./images/slider.png')}
          style={{height: 190, width: '100%', borderRadius: 7}}
        />
      </View>
      <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
        <TouchableOpacity style={{width: 50}}>
          <View style={styles.logo}>
            <Image source={require('./images/path.png')} />
          </View>
          <Text style={{textAlign: 'center'}}>Man</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width: 50}}>
          <View style={styles.logo}>
            <Image source={require('./images/path.png')} />
          </View>
          <Text style={{textAlign: 'center'}}>Woman</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width: 50}}>
          <View style={styles.logo}>
            <Image source={require('./images/path.png')} />
          </View>
          <Text style={{textAlign: 'center'}}>Kids</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width: 50}}>
          <View style={styles.logo}>
            <Image source={require('./images/path.png')} />
          </View>
          <Text style={{textAlign: 'center'}}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width: 50}}>
          <View style={styles.logo}>
            <Image source={require('./images/path.png')} />
          </View>
          <Text style={{textAlign: 'center'}}>More</Text>
        </TouchableOpacity>
      </View>
     
      <View>
        <Text
          style={{
            fontSize: 26,
            fontWeight: '600',
            fontFamily: 'monserrat',
            color: '#4D4D4D',
            marginVertical: 15,
          }}>
          Flash Sale
        </Text>
      </View>
      <ScrollView horizontal={true}>
        <View
          style={{height: 170, width: 120, borderRadius: 5, borderWidth: 0.5,  marginRight: 10}}>
          <Image
            source={require('./images/sabun.png')}
            style={{height: 117, width: 120}}
          />
          <View style={{margin: 10}}>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '500',
                fontFamily: 'monserrat',
                color: '#575757',
              }}>
              Tiare Handwash
            </Text>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '600',
                fontFamily: 'monserrat',
                color: '#323232',
              }}>
              $12.22
            </Text>
          </View>
        </View>

        <View
          style={{height: 170, width: 120, borderRadius: 5, borderWidth: 0.5, marginRight: 10}}>
          <Image
            source={require('./images/jbl.png')}
            style={{height: 117, width: 120}}
          />
          <View style={{margin: 10}}>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '500',
                fontFamily: 'monserrat',
                color: '#575757',
              }}>
              JBL Speaker
            </Text>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '600',
                fontFamily: 'monserrat',
                color: '#323232',
              }}>
              $12.22
            </Text>
          </View>
        </View>

        <View
          style={{height: 170, width: 120, borderRadius: 5, borderWidth: 0.5, marginRight: 10}}>
          <Image
            source={require('./images/googlehome.png')}
            style={{height: 117, width: 120}}
          />
          <View style={{margin: 10}}>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '500',
                fontFamily: 'monserrat',
                color: '#575757',
              }}>
              Google Home
            </Text>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '600',
                fontFamily: 'monserrat',
                color: '#323232',
              }}>
              $80.30
            </Text>
          </View>
        </View>
      </ScrollView>

 
      <View>
        <Text
          style={{
            fontSize: 26,
            fontWeight: '600',
            fontFamily: 'monserrat',
            color: '#4D4D4D',
            marginVertical: 15,
          }}>
          New Product
        </Text>
      </View>        

      <ScrollView horizontal={true}>
        <View
          style={{height: 170, width: 120, borderRadius: 5, borderWidth: 0.5,  marginRight: 10}}>
          <Image
            source={require('./images/sabun.png')}
            style={{height: 117, width: 120}}
          />
          <View style={{margin: 10}}>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '500',
                fontFamily: 'monserrat',
                color: '#575757',
              }}>
              Tiare Handwash
            </Text>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '600',
                fontFamily: 'monserrat',
                color: '#323232',
              }}>
              $12.22
            </Text>
          </View>
        </View>

        <View
          style={{height: 170, width: 120, borderRadius: 5, borderWidth: 0.5, marginRight: 10}}>
          <Image
            source={require('./images/jbl.png')}
            style={{height: 117, width: 120}}
          />
          <View style={{margin: 10}}>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '500',
                fontFamily: 'monserrat',
                color: '#575757',
              }}>
              JBL Speaker
            </Text>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '600',
                fontFamily: 'monserrat',
                color: '#323232',
              }}>
              $12.22
            </Text>
          </View>
        </View>

        <View
          style={{height: 170, width: 120, borderRadius: 5, borderWidth: 0.5, marginRight: 10}}>
          <Image
            source={require('./images/googlehome.png')}
            style={{height: 117, width: 120}}
          />
          <View style={{margin: 10}}>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '500',
                fontFamily: 'monserrat',
                color: '#575757',
              }}>
              Google Home
            </Text>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '600',
                fontFamily: 'monserrat',
                color: '#323232',
              }}>
              $80.30
            </Text>
          </View>
        </View>
      </ScrollView>
    </ScrollView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
    paddingHorizontal:10
  },
  logo: {
    backgroundColor: '#FFBC6C',
    height: 50,
    width: 50,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
