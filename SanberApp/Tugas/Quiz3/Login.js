import React from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from 'react-native'
import { AntDesign } from '@expo/vector-icons';

const Login = ({navigation}) => {
    return (
        <View style={styles.container}>
            <View style={{marginBottom: 30}}>
                <Text style={{fontWeight: 'bold', fontSize: 30,}}>Welcome Back</Text>
                <Text>Sign in to continue</Text>
            </View>
            <View style={styles.body}>
                <View style={{marginTop: 30}}>
                    <View style={{marginBottom: 20}}>
                        <Text>Email</Text>
                        <TextInput
                            style={styles.textInput}
                            underlineColorAndroid='black'>
                        </TextInput>
                    </View>
                    <View style={{marginBottom: 20}}>
                        <Text>Password</Text>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <TextInput
                                style={styles.textInput}
                                underlineColorAndroid='black'>
                            </TextInput>
                            <AntDesign name="eye" size={24} color="black"/>
                        </View>
                    </View>
                    <Text style={{alignSelf: 'flex-end'}}>Forgot Password?</Text>
                    <View style={{flexDirection: 'column'}}>
                        <TouchableOpacity onPress={() => navigation.navigate('MainApp')} style={styles.signUpButton}>
                            <Text style={styles.signUpText}>Sign In</Text>
                        </TouchableOpacity>
                        <Text style={{alignSelf: 'center', marginTop: 25, fontSize: 20}}>-OR-</Text>
                        <View style={{marginTop: 20, flexDirection: 'row', justifyContent: 'space-evenly'}}>
                            <View style={styles.boxLogin}>
                                <Image style={styles.iconlogin} source={require('./images/fb.png')}/>
                                <Text style={{marginLeft: 10, fontWeight: 'bold'}}>Facebook</Text>
                            </View>
                            <View style={styles.boxLogin}>
                                <Image style={styles.iconlogin} source={require('./images/google.png')}/>
                                <Text style={{marginLeft: 10, fontWeight: 'bold'}}>Google</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 20,
        marginTop: 20,
    },
    body: {
        height: 500,
        padding: 20,
        elevation: 2,
        borderWidth: 0.1,
        marginRight: 20,
        borderColor: 'grey',
        borderRadius: 10,
    },
    textInput: {
        color: 'black',
        height: 40,
        width: 250,
    },
    signUpButton: {
        marginTop: 40,
        backgroundColor: '#FF5349',
        marginRight: 15,
        height: 50,
        borderRadius: 5,
    },
    signUpText: {
        color: 'white',
        alignSelf: 'center',
        marginTop: 15,
        fontWeight: 'bold',
    },
    boxLogin: {
        flexDirection: 'row',
        borderWidth: 0.1,
        width: 130,
        alignItems: 'center',
        height: 50,
        borderRadius: 2,
    },
    iconlogin: {
        height: 20,
        width: 20,
        marginLeft: 15,
        borderRadius: 5,
    }
})
