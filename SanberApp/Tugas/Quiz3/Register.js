import React from 'react'
import { StyleSheet, Text, TextInput, View, TouchableOpacity } from 'react-native'

const Register = ({navigation}) => {
    return (
        <View style={styles.container}>
            <View style={{marginBottom: 30}}>
                <Text style={{fontWeight: 'bold', fontSize: 30,}}>Welcome</Text>
                <Text>Sign up to continue</Text>
            </View>
            <View style={styles.body}>
                <View style={{marginTop: 30}}>
                    <View style={{marginBottom: 20}}>
                        <Text>Name</Text>
                        <TextInput
                            style={styles.textInput}
                            underlineColorAndroid='black'>
                        </TextInput>
                    </View>
                    <View style={{marginBottom: 20}}>
                        <Text>Email</Text>
                        <TextInput
                            style={styles.textInput}
                            underlineColorAndroid='black'>
                        </TextInput>
                    </View>
                    <View style={{marginBottom: 20}}>
                        <Text>Phone Number</Text>
                        <TextInput
                            style={styles.textInput}
                            underlineColorAndroid='black'>
                        </TextInput>
                    </View>
                    <View style={{marginBottom: 20}}>
                        <Text>Password</Text>
                        <TextInput
                            style={styles.textInput}
                            underlineColorAndroid='black'>
                        </TextInput>
                    </View>
                </View>
                
                <View style={{flexDirection: 'column'}}>
                    <TouchableOpacity onPress={() => navigation.navigate('Login')} style={styles.signUpButton}>
                        <Text style={styles.signUpText}>Sign Up</Text>
                    </TouchableOpacity>
                    <View style={{flexDirection: 'row', marginTop: 8, justifyContent: 'center'}}>
                        <Text>Already Have an account? </Text>
                        <Text onPress={() => navigation.navigate('Login')} style={{color: '#FF5349'}}> Sign In</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default Register

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 20,
        marginTop: 20,
    },
    textInput: {
        color: 'black',
        height: 40,
        marginRight: 15,
    },
    signUpButton: {
        backgroundColor: '#FF5349',
        marginRight: 15,
        height: 50,
        borderRadius: 5,
    },
    signUpText: {
        color: 'white',
        alignSelf: 'center',
        marginTop: 15,
        fontWeight: 'bold',
    },
    body: {
        height: 500,
        padding: 20,
        elevation: 2,
        borderWidth: 0.1,
        marginRight: 5,
        borderColor: 'grey',
        borderRadius: 10,
    },
})
