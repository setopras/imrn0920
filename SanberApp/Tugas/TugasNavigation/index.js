import React from "react";
import { NavigationContainer} from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import LoginScreen from './LoginScreen';
import SkillScreen from './SkillScreen';
import AboutScreen from './AboutScreen';
import ProjectScreen from './ProjectScreen.js';
import AddScreen from './AddScreen';

// const Stack = createStackNavigator()
const Tabs = createBottomTabNavigator()
const Drawer = createDrawerNavigator()

// const index = () => {
//     return (
//         // <NavigationContainer>
//             <Stack.Navigator initialRouteName={LoginScreen}>
//                 <Stack.Screen name="LoginScreen" component={LoginScreen}/>
//                 {/* <Stack.Screen name="AboutScreen" component={AboutScreen}/>
//                 <Stack.Screen name="MainApp" component={MainApp}/> */}
//                 {/* <Stack.Screen name="DrawerScreen" component={DrawerScreen}/> */}
//             </Stack.Navigator> 
//         // </NavigationContainer>
//     )
// }

const MainApp = () => {
    return (
        <Tabs.Navigator>
            <Tabs.Screen name="SkillScreen" component={SkillScreen}/>
            <Tabs.Screen name="AboutScreen" component={AboutScreen}/>
            <Tabs.Screen name="ProjectScreen" component={ProjectScreen}/>
            <Tabs.Screen name="AddScreen" component={AddScreen}/>
        </Tabs.Navigator>
    )
}

export default () => {
    return(
        <NavigationContainer>
            <Drawer.Navigator initialRouteName={LoginScreen}>
                <Drawer.Screen name="LoginScreen" component={LoginScreen} />   
                <Drawer.Screen name="MainApp" component={MainApp} />         
                <Drawer.Screen name="AboutScreen" component={AboutScreen} />
            </Drawer.Navigator>
        </NavigationContainer>
        
    )
}

const DrawerScreen = ()=>{
    return(
    <Drawer.Navigator>
        <Drawer.Screen name="MainApp" component={MainApp} />
        <Drawer.Screen name="AboutScreen" component={AboutScreen} />
    </Drawer.Navigator>
    )
}
