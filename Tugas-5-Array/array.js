console.log("TUGAS ARRAY")
console.log(" ")
console.log("========= Pembatas ==========")
console.log(" ")
console.log("SOAL NO.1")
console.log(" ")



function range(startNum, finishNum){
    var array = [];
   
    if (startNum>=finishNum){
        for (var x=startNum; x>=finishNum; x--){
            array.push(x);
        }
    } else if(startNum<=finishNum){
        for (var y=startNum; y<=finishNum; y++){
            array.push(y);
        }
    } else if(startNum == 1){
        return -1
    } else {
        return -1
    } 
    return array;
}
console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());


console.log(" ")
console.log("========= Pembatas ==========")
console.log(" ")
console.log("SOAL NO.2")
console.log(" ")


function rangeWithStep(startNum, finishNum, step){
    var array2=[];

    if (startNum<=finishNum){
        for(var X = startNum; X<=finishNum; X+=step){
            array2.push(X);
        }
    } else {
        for(var X = startNum; X>=finishNum; X-=step){
            array2.push(X);
        }
    } 

    return array2;
}
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));



console.log(" ")
console.log("========= Pembatas ==========")
console.log(" ")
console.log("SOAL NO.3")
console.log(" ")



function sum(startNum, finishNum, step=1){
    var array3=[];

    if(startNum<=finishNum){
        for(var X = startNum; X <= finishNum; X+=step){
            array3.push(X);
        }   
    }else if (startNum>=finishNum){
        for(var X=startNum; X>=finishNum; X-=step){
            array3.push(X);
        }
    }else if(startNum){
        return startNum;
    }else {
        return 0;
    }
    var total = 0;

    for(var Y = 0; Y<array3.length; Y++){
        total = total + array3[Y];
    }
    return total;
}

console.log(sum(1,10)) 
console.log(sum(5, 50, 2)) 
console.log(sum(15,10)) 
console.log(sum(20, 10, 2)) 
console.log(sum(1)) 
console.log(sum()) 



console.log(" ")
console.log("========= Pembatas ==========")
console.log(" ")
console.log("SOAL NO.4")
console.log(" ")


var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"], 
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"], 
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"], 
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"] 
    ];

function dataHandling(data) {
    var panjangData = data.length
    for (var X = 0; X < panjangData; X++) {
        var id = 'Nomor ID: ' + data[X][0]
        var nama = 'Nama Lengkap: ' + data[X][1]
        var ttl = 'TTL: ' + data[X][2] + ' ' + data[X][3]
        var hobi = 'Hobi: ' + data[X][4]
        console.log(id)
        console.log(nama)
        console.log(ttl)
        console.log(hobi)
        console.log()
    } 
}

dataHandling(input);


console.log(" ")
console.log("========= Pembatas ==========")
console.log(" ")
console.log("SOAL NO.5")
console.log(" ")


function balikKata(kata) {
   
    var kataBaru = '';
    for (var X = kata.length - 1; X >= 0; X--) { 
        kataBaru += kata[X]
    }
    return kataBaru;
}

  console.log(balikKata("Kasur Rusak")) 
  console.log(balikKata("SanberCode")) 
  console.log(balikKata("Haji Ijah")) 
  console.log(balikKata("racecar")) 
  console.log(balikKata("I am Sanbers"))  



  console.log(" ")
  console.log("========= Pembatas ==========")
  console.log(" ")
  console.log("SOAL NO.6")
  console.log(" ")



function dataHandling2(array) {
    array.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro");
    console.log(array);
    let date = array[3].split("/");
    switch(date[1]) {
      case "01":
      case 01:
        var bulan = "Januari";
        break;
      case "02":
      case 02:
        var bulan = "Februari";
        break;
      case "03":
      case 03:
        var bulan = "Maret";
        break;
      case "04":
      case 04:
        var bulan = "April";
        break;
      case "05":
      case 05:
        var bulan = "Mei";
        break;
      case "06":
      case 06:
        var bulan = "Juni";
        break;
      case "07":
      case 07:
        var bulan = "Juli";
        break;
      case "08":
      case 08:
        var bulan = "Agustus";
        break;
      case "09":
      case 09:
        var bulan = "September";
        break;
      case "10":
      case 10:
        var bulan = "Oktober";
        break;
      case "11":
      case 11:
        var bulan = "November";
        break;
      case "12":
      case 12:
        var bulan = "Desember";
        break;
    }
    console.log(date);
    console.log(bulan);
    let sorting = array[3].split("/").sort((a, b) => b - a);
    console.log(sorting);
    date = date.join("-");
    console.log(date);
    let slicing = array[1].slice(0, 15);
    console.log(slicing)
  }
  
  var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
  
  
  dataHandling2(input);
  
  
  console.log(" ")
  console.log(" ")