console.log(" ")
console.log(" ")
console.log("TUGAS OBJECT")
console.log(" ")
console.log("========= Pembatas ==========")
console.log(" ")
console.log("SOAL NO.1")
console.log(" ")



function arrayToObject(arr) {
    var text = "";
    for (i = 0; i < arr.length; i++) {

    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    var umur;
        
    if (arr[i][3] != "undefined" && thisYear > arr[i][3]) {
        umur = thisYear - arr[i][3]
        } else {
        umur = "Invalid Birth Year";
        }
        
    var obj =
        {
        firstName: arr[i][0],
        lastName: arr[i][1],
        gender: arr[i][2],
        age: umur,
        }
        
    var myText = JSON.stringify(obj);
        text += (i + 1) + ". " + obj.firstName + " " + obj.lastName + " : " + myText + "\n";
    }
    return text;
}
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female",2023]]
console.log(arrayToObject(people))
console.log(arrayToObject(people2))

 //Error case 
arrayToObject([])



console.log(" ")
console.log("========= Pembatas ==========")
console.log(" ")
console.log("SOAL NO.2")
console.log(" ")


function shoppingTime(memberId, money) {
    if(!memberId){
        return " Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000){
        return "Mohon Maaf , uang tidak cukup"
    }else {
        var newObject ={}
        var moneyChange = money;
        var purchaseList = [];
        var sepatuStacattu ="Sepatu stacatu";
        var bajuZoro = "Baju zoro";
        var bajuHn= "Baju H&N"
        var sweaterUniklooh = "Sweater Unikloh";
        var casingHandphone = "Casing Handphone";
        
        var check = 0;
        for (var i = 0; moneyChange >= 50000  && check ==0; i++){
            if( moneyChange >= 1500000){
                purchaseList.push(sepatuStacattu)
                moneyChange -= 1500000
            }else if( moneyChange >= 500000){
                purchaseList.push(bajuZoro)
                moneyChange -= 500000
            }else if( moneyChange >= 250000){
                purchaseList.push(bajuHn)
                moneyChange -= 250000
            }else if( moneyChange >= 175000){
                purchaseList.push(sweaterUniklooh)
                moneyChange -= 175000
            }else if( moneyChange >= 50000){
               for (var j= 0; j<= purchaseList.length-1; j++){
                   if(purchaseList[j]== casingHandphone){
                       check +=1
                   }
               }if(check==0){
                   purchaseList.push(casingHandphone)
                   moneyChange -= 50000
               }else{
                   purchaseList.push(casingHandphone)
                   moneyChange -=50000
               }
            }

        }
        newObject.memberId = memberId
        newObject.money = money
        newObject.listPurchased = purchaseList
        newObject.changeMoney = moneyChange
        return newObject
    }
  }
   
// TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


  console.log(" ")
console.log("========= Pembatas ==========")
console.log(" ")
console.log("SOAL NO.3")
console.log(" ")



function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    if(arrPenumpang[0] == null) {
        return []
    } else {
        var hasil = []
        for(i=0; i<arrPenumpang.length; i++) {
        var penumpang = arrPenumpang[i][0]
        var naikDari = arrPenumpang[i][1]
        var tujuan = arrPenumpang[i][2]
        var bayar = (rute.indexOf(tujuan) - rute.indexOf(naikDari)) * 2000
        var obj = {
            penumpang: penumpang,
            naikDari: naikDari,
            tujuan: tujuan,
            bayar: bayar
        }
        hasil.push(obj)
        }
        return hasil
    }
  }
  
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
   
  console.log(naikAngkot([])); //[]


  console.log(" ")
  console.log(" ")
