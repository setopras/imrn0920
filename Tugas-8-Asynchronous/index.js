console.log(" ")
console.log("TUGAS ASYNCHRONOUS")
console.log(" ")
console.log("========= Pembatas ==========")
console.log(" ")
console.log("SOAL NO.1")
console.log(" ")

// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 

readBooks(10000, books[0], waktu => {
    readBooks(waktu, books[1], waktu1 => {
        readBooks(waktu1, books[2], waktu2 => {
            return waktu2;
        })
    })
})



console.log(" ")
console.log(" ")