console.log(" ")
console.log("========= Pembatas ==========")
console.log(" ")
console.log("SOAL NO.2")
console.log(" ")

var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise 


function bacaBuku(timespent, waktu) {
    if (waktu >= books.length) {
      return;
    }
    readBooksPromise(timespent, books[waktu]).then(sisaWaktu =>
      bacaBuku(sisaWaktu, waktu + 1)
    );
  }
  
  bacaBuku(10000, 0);

  console.log(" ")
  console.log(" ")
  
