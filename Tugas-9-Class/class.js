console.log(" ")
console.log("TUGAS Class")
console.log(" ")
console.log("========= Pembatas ==========")
console.log(" ")
console.log("SOAL NO.1")
console.log(" ")
console.log("RELEASE 0")
console.log(" ")



class Animal {
  constructor(name){
      this.name=name;
      this.legs=4;
      this.cold_blooded=false;
  }
}
var sheep = new Animal("shaun");

console.log(sheep.name) 
console.log(sheep.legs) 
console.log(sheep.cold_blooded) 



console.log(" ")
console.log("RELEASE 1")
console.log(" ")


class Frog extends Animal {
  constructor(namaHewan) {
      super(namaHewan)
  }
  jump = () =>
      console.log(`hop hop`)

}

class Ape extends Animal {
  constructor(namaHewan) {
      super(namaHewan)
  }
  yell = () =>
      console.log(`Auooo`)

}

var sungokong = new Ape("kera sakti")
// console.log(sungokong)
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
// console.log(kodok)
kodok.jump() // "hop hop" 


console.log(" ")
console.log("========= Pembatas ==========")
console.log(" ")
console.log("SOAL NO.2")
console.log(" ")

class Clock {
  constructor({ template }) {
      this.template = template
      this.timer = 0
  }
  render = () => {
      var date = new Date();

      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;

      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;

      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;

      var output = this.template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);

      console.log(output);
  }

  stop = () => {
      clearInterval(this.timer)
  }
  start = () => {
      this.render()
      this.timer = setInterval(this.render, 1000)
  }
}
var clock = new Clock({ template: 'h:m:s' });
clock.start();

console.log(" ")
console.log(" ")